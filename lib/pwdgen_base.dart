import 'dart:math' ;
import 'words.dart' ;
var rng = Random() ;
var allwords = beings + words + mythology + science + literature ;
class PwdGen {
  String _separator = '_' ;
  void SetSeparator(String newval) {
    _separator = newval ;
  }
  String Random ({segments=2}) {
    String result = '';
    for (var i=0; i<segments; i++) {
      result += allwords[rng.nextInt(allwords.length - 1)] + _separator + rng.nextInt(10000).toRadixString(16) ;
    }
    return result ;
  }
}