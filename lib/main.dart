// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'pwdgen_base.dart' ;

void main() {
  runApp(const MyApp());
}

// #docregion MyApp
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // #docregion build
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Passwords'),
        ),
        body: const Center(
          child: Passwords(),
        ),
      ),
    );
  }
// #enddocregion build
}
// #enddocregion MyApp

// #docregion RandomWordsWidget
// #docregion RandomWords
class Passwords extends StatefulWidget {
  const Passwords({super.key});

  @override
  State<Passwords> createState() => _PasswordsState();
}
// #enddocregion RandomWords

// #docregion _RandomWordsState, RWS-class-only
class _PasswordsState extends State<Passwords> {
  // #enddocregion RWS-class-only
  var pwd = PwdGen() ;
  var len = 0 ;
  List<String> passwords =[]; // = ['abc']; // = String[];                 // NEW
  Set<String> _favorites = {} ;
  //var String passwords[128] ;
  final _biggerFont = const TextStyle(fontSize: 18); // NEW

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemBuilder: (context, i) {
          //if (i.isOdd) return const Divider();
          if (i >= passwords.length) {
            passwords.add(pwd.Random()) ;
          }
          final bool alreadySaved = _favorites.contains(passwords[i]) ;
          return ListTile(
            title: Text(
              //i.toString() + ' -> ' +
              passwords[i] ,
              style: _biggerFont,
            ),
            trailing: Icon(
            alreadySaved ? Icons.favorite : Icons.favorite_border,
            color: alreadySaved ? Colors.red : Colors.green ,
            semanticLabel: alreadySaved ? 'Remove from saved' : 'Save',
            ),
            onTap: () {
              setState(() {
              if (alreadySaved) {
              _favorites.remove(passwords[i]);
              } else {
                _favorites.add(passwords[i]);
              }
            }); });
        });

  }
}
